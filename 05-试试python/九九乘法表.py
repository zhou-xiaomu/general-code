# 方法一：
i=1
while i<10:
    j=1
    while j<=i:
        print(f'{i}*{j}={i*j}',end='\t')
        j += 1
    print(' ')
    i += 1

# 方法二：
#for i in range(1,10):
#    for j in range(1,i+1) for i in range(1,10):
#       print(f'{i}*{j}={i*j}\t',end='')
#    print(' ')