$.getJSON('http://127.0.0.1:8080/info', function (json) {

    $('#name').text(json.name)
    $("#intro").text(json.intro)
    $("#about").text(json.about)
  
    //0为女生，1为男生
    //如果是0，if中为假，不成立，输出女孩表情；为1，则反之
    if (json.sex == '1') {
      $("#sex").text('👦')
    } else {
      $("#sex").text('👧')
    }
  
  })