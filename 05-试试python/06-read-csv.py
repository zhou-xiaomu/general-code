import csv

data_path = 'data.csv'

# 打开csv文件
file_csv = open(data_path, 'r', encoding='utf-8')

# 将内容解析出来
object_csv = csv.reader(file_csv) 

# 关闭文件
file_csv.close()