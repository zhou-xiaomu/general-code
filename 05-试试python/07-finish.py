import csv
from flask import Flask, request

server = Flask('app')

data_path = 'data.csv'

@server.route('/api/info')
def get_info():
  # 获得url中的id
  user_id = request.args.get('id')

  # 打开文件
  file = open(data_path, 'r', encoding='utf-8')
  # 转换为列表
  data = csv.reader(file)

  for item in data:
    if item[0] == user_id:
      return {
          "name": item[1],
          "sex": item[2],
          "intro": item[3],
          "about": item[4]
      }

  # 关闭文件
  file.close()

  # 没有匹配，则返回空
  return {
    "name": None,
    "sex": None,
    "intro": None,
    "about": None
  }

# 开启服务
server.run('127.0.0.1', port=8080)