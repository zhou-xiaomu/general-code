from flask import Flask

# 新建服务
server = Flask('app')

#发送的数据
data = { 
    "name": "周沐",
    "sex": 1,
    "intro": "大一学生，就读于沈阳理工大学计算机专业。",
    "about": "身体健康，大脑健全，心态良好，外貌姣好，性格开朗，反诈骗能力高。"
}

@server.route('/')
def index():
  return 'Hello Flask!'

@server.route('/info')
def hanshuming():
  return data

server.run('127.0.0.1',port=8080)