import flask

server = flask.Flask('app')
#相当于:
#from flask import Flask
#serve = Flask('app')
@server.route('/')
def index():
    return'<h1>hello flask!</h1>'
server.run()
