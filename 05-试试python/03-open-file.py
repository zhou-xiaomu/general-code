import json

a_file = open("info.json",'r',encoding='utf-8')
#'r'表示读，'w'表示写，'a'表示添加
a_file_content = a_file.read()
a_file_json = json.loads(a_file_content)

print(a_file_json)
print(type(a_file_json))

a_file.close()