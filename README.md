# 一篇笔记
| 快捷键 | 作用 | 
| ----   |----|
|ALT+shift+f|格式化文档|
|CTRL+c|复制|
|CTRL+v|粘贴|
|Win+shift+s|截屏|
|Win+CTRL+左/右|切屏|
|CTRL+ENTER|vscode中另起一行|
|CTRL+shift+c|浏览器中呼叫控制台|
|clear（）+ENTER|清屏|
|>|markdown引用|
|WIN+。|emjoy表情|
|CTRL+s|保存|

>emmm,再写一些什么呢

# 选择器
## h1 {color:blue;font-size:12px;}
>h1表示选择器，{}中内容用来修饰选择的内容

>{}中内容为声明

>color和font-size表示属性

>blue和12px表示值
# 盒模型
## Margin Border Padding Content 

# cmd命令
|输入|含义|
|---|---|
|ipconfig| 本机网络信息|
|mkdir xxx|创建文件夹|
|rd /s /q xxx|删除文件夹|
|del xxx|删除文件|
|shutdown -s |关机|
|shutdown -s -t 30 |三十秒后关机|
|shutdown -a|解除命令|
|notepad|打开记事本|
|winver|查看Windows版本|
|winmsd|系统信息|
|scannow|扫描错误并复原|
|write|写字板|
|mmc|打开控制台|
|color f0(/其他符号)|更换颜色|

